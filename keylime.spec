%global srcname keylime

# Package is actually noarch, but it has an optional dependency that is
# arch-specific.
%global debug_package %{nil}

Name:    keylime
Version: 6.4.2
Release: 2%{?dist}
Summary: Open source TPM software for Bootstrapping and Maintaining Trust

URL:            https://github.com/keylime/keylime
Source0:        https://github.com/keylime/keylime/archive/refs/tags/v%{version}.tar.gz
Source1:        %{srcname}.sysusers

License: ASL 2.0 and MIT

BuildRequires: git-core
BuildRequires: swig
BuildRequires: openssl-devel
BuildRequires: python3-devel
BuildRequires: python3-dbus
BuildRequires: python3-setuptools
BuildRequires: systemd-rpm-macros

Requires: python3-%{srcname} = %{version}-%{release}
Requires: %{srcname}-base = %{version}-%{release}
Requires: %{srcname}-verifier = %{version}-%{release}
Requires: %{srcname}-registrar = %{version}-%{release}
Requires: %{srcname}-tenant = %{version}-%{release}

# Agent.
Requires: keylime-agent
Suggests: keylime-agent-rust

%{?python_enable_dependency_generator}
%description
Keylime is a TPM based highly scalable remote boot attestation
and runtime integrity measurement solution.

%package base
Summary: The base package contains the default configuration
License: MIT


Requires(pre): shadow-utils
Requires: procps-ng
Requires: tpm2-tss
Requires: tpm2-tools

%ifarch %efi
Requires: efivar-libs
%endif


%description base
The base package contains the Keylime default configuration

%package -n python3-%{srcname}
Summary: The Python Keylime module
License: MIT

Requires: %{srcname}-base = %{version}-%{release}
%{?python_provide:%python_provide python3-%{srcname}}

Requires: python3-tornado
Requires: python3-sqlalchemy
Requires: python3-alembic
Requires: python3-cryptography
Requires: python3-pyyaml
Requires: python3-packaging
Requires: python3-requests
Requires: python3-gpg
Requires: python3-lark-parser


%description -n python3-%{srcname}
The python3-keylime module implements the functionality used
by Keylime components.

%package verifier
Summary: The Python Keylime Verifier component
License: MIT

Requires: %{srcname}-base = %{version}-%{release}
Requires: python3-%{srcname} = %{version}-%{release}

%description verifier
The Keylime Verifier continuously verifies the integrity state
of the machine that the agent is running on.

%package registrar
Summary: The Keylime Registrar component
License: MIT

Requires: %{srcname}-base = %{version}-%{release}
Requires: python3-%{srcname} = %{version}-%{release}

%description registrar
The Keylime Registrar is a database of all agents registered
with Keylime and hosts the public keys of the TPM vendors.

%package tenant
Summary: The Python Keylime Tenant
License: MIT

Requires: %{srcname}-base = %{version}-%{release}
Requires: python3-%{srcname} = %{version}-%{release}


%description tenant
The Keylime Tenant can be used to provision a Keylime Agent.

%prep
%autosetup -S git -n %{srcname}-%{version}

%build
%py3_build

%install
%py3_install
mkdir -p %{buildroot}/%{_sharedstatedir}/%{srcname}
mkdir -p --mode=0700 %{buildroot}/%{_rundir}/%{srcname}
mkdir -p --mode=0700 %{buildroot}/%{_localstatedir}/log/%{srcname}

# Remove agent and webapp.
rm -f %{buildroot}/%{_bindir}/%{srcname}_agent
rm -f %{buildroot}%{python3_sitelib}/%{srcname}/__pycache__/%{srcname}_agent*
rm -f %{buildroot}%{python3_sitelib}/%{srcname}/cmd/__pycache__/agent.*
rm -f %{buildroot}%{python3_sitelib}/%{srcname}/cmd/agent.*
rm -f %{buildroot}%{python3_sitelib}/%{srcname}/%{srcname}_agent.*

rm -f %{buildroot}/%{_bindir}/%{srcname}_webapp
rm -f %{buildroot}%{python3_sitelib}/%{srcname}/__pycache__/tenant_webapp.*
rm -f %{buildroot}%{python3_sitelib}/%{srcname}/cmd/__pycache__/webapp.*
rm -f %{buildroot}%{python3_sitelib}/%{srcname}/cmd/webapp.*
rm -f %{buildroot}%{python3_sitelib}/%{srcname}/tenant_webapp.*
rm -rf %{buildroot}%{python3_sitelib}/%{srcname}/static/

# Remove misc progs.
rm -f %{buildroot}/%{_bindir}/%{srcname}_ima_emulator
rm -f %{buildroot}/%{_bindir}/%{srcname}_userdata_encrypt

# Setting up the agent to use keylime:keylime user/group after dropping privileges.
sed -e 's/^run_as[[:space:]]*=.*/run_as = keylime:keylime/g' -i %{srcname}.conf

# Using sha256 for tpm_hash_alg.
sed -e 's/^tpm_hash_alg[[:space:]]*=.*/tpm_hash_alg = sha256/g' -i %{srcname}.conf

install -Dpm 600 %{srcname}.conf \
    %{buildroot}%{_sysconfdir}/%{srcname}.conf

install -Dpm 644 ./services/%{srcname}_verifier.service \
    %{buildroot}%{_unitdir}/%{srcname}_verifier.service

install -Dpm 644 ./services/%{srcname}_registrar.service \
    %{buildroot}%{_unitdir}/%{srcname}_registrar.service

cp -r ./tpm_cert_store %{buildroot}%{_sharedstatedir}/keylime/

install -p -d %{buildroot}/%{_tmpfilesdir}
cat > %{buildroot}/%{_tmpfilesdir}/%{srcname}.conf << EOF
d %{_rundir}/%{srcname} 0700 %{srcname} %{srcname} -
EOF

install -p -D -m 0644 %{SOURCE1} %{buildroot}%{_sysusersdir}/%{srcname}.conf

%pre base
%sysusers_create_compat %{SOURCE1}
exit 0

%posttrans base
[ -f %{_sysconfdir}/%{srcname}.conf ] && \
    chmod 600 %{_sysconfdir}/%{srcname}.conf && \
    chown %{srcname} %{_sysconfdir}/%{srcname}.conf
[ -d %{_sharedstatedir}/%{srcname} ] && \
    chown -R %{srcname} %{_sharedstatedir}/%{srcname}/
[ -d %{_localstatedir}/log/%{srcname} ] && \
    chown -R %{srcname} %{_localstatedir}/log/%{srcname}/
exit 0

%post verifier
%systemd_post %{srcname}_verifier.service

%post registrar
%systemd_post %{srcname}_registrar.service

%preun verifier
%systemd_preun %{srcname}_verifier.service

%preun registrar
%systemd_preun %{srcname}_registrar.service

%postun verifier
%systemd_postun_with_restart %{srcname}_verifier.service

%postun registrar
%systemd_postun_with_restart %{srcname}_registrar.service

%files verifier
%license LICENSE
%{_bindir}/%{srcname}_verifier
%{_bindir}/%{srcname}_ca
%{_bindir}/%{srcname}_migrations_apply
%{_unitdir}/keylime_verifier.service

%files registrar
%license LICENSE
%{_bindir}/%{srcname}_registrar
%{_unitdir}/keylime_registrar.service

%files tenant
%license LICENSE
%{_bindir}/%{srcname}_tenant

%files -n python3-%{srcname}
%license LICENSE
%{python3_sitelib}/%{srcname}-*.egg-info/
%{python3_sitelib}/%{srcname}

%files base
%license LICENSE
%doc README.md
%config(noreplace) %attr(600,%{srcname},%{srcname}) %{_sysconfdir}/%{srcname}.conf
%attr(700,%{srcname},%{srcname}) %dir %{_rundir}/%{srcname}
%attr(700,%{srcname},%{srcname}) %dir %{_localstatedir}/log/%{srcname}
%attr(700,%{srcname},%{srcname}) %{_sharedstatedir}/%{srcname}
%{_tmpfilesdir}/%{srcname}.conf
%{_sysusersdir}/%{srcname}.conf

%files
%license LICENSE

%changelog
* Mon Jul 11 2022 Sergio Correia <scorreia@redhat.com> - 6.4.2-2
- Fix efivar-libs dependency
  Related: rhbz#2082989

* Thu Jul 07 2022 Sergio Correia <scorreia@redhat.com> - 6.4.2-1
- Update to 6.4.2
  Related: rhbz#2082989

* Tue Jun 21 2022 Sergio Correia <scorreia@redhat.com> - 6.4.1-1
- Add keylime to RHEL-9
  Resolves: rhbz#2082989
